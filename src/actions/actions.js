import { push } from 'connected-react-router';

const addUser = user => ({
  type: 'ADD_USER',
  user: user
})

const addLoginedUser = user => ({
  type: 'ADD_LOGINED_USER',
  user: user
})

export const asyncAddUser = values => {
  return (dispatch, getState) => {
    setTimeout( () => {
        dispatch(addUser(values))
        dispatch(push("/login"))
    }, 3000 );
  }
}

export const asyncLogin = values => {
  return (dispatch, getState) => {
    setTimeout( () => {
      const state = getState()
      for (const user of state.users) {
        if( user.email === values.email && user.password === values.password ) {
          console.log("login succeful!!!",values)
          dispatch(addLoginedUser(user))
          dispatch(push("/"))
          return
        }
      }
      console.log("login failed!!!",values)
    }, 3000 );
  }
}
