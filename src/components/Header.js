import React from 'react';
import { Link } from "react-router-dom";
import { Breadcrumb } from 'antd';

function Header() {
  return (
    <Breadcrumb>
      <Breadcrumb.Item>
        <Link to="/">ホーム</Link>
      </Breadcrumb.Item>
      <Breadcrumb.Item>
        <Link to="/login">ログイン</Link>
      </Breadcrumb.Item>
      <Breadcrumb.Item>
        <Link to="/register">ユーザ登録</Link>
      </Breadcrumb.Item>
    </Breadcrumb>
  );
}

export default Header;
