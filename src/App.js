import React, { Component } from 'react'
import styled from 'styled-components'
import { Switch, Route } from 'react-router-dom'

import Home from './containers/Home/Loadable'
import LoginPage from './containers/Login/Loadable'
import RegisterPage from './containers/Register/Loadable'
import NotFoundPage from './containers/NotFoundPage/Loadable'
import Header from './components/Header'
import Footer from './components/Footer'

import 'antd/dist/antd.css';
import './form-style.css';

const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
  background: papayawhip;
  .btn {
    line-height: 0;
  }
`;

// eslint-disable-next-line
class App extends Component {
  render() {
    return (
      <AppWrapper>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/register" component={RegisterPage} />
          <Route path="" component={NotFoundPage} />
        </Switch>
        <Footer />
      </AppWrapper>
    );
  }
}

export default App;
