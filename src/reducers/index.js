export const users = (state = [], action) => {
  switch (action.type) {
    case 'ADD_USER': // *** userを追加 
      return [
        ...state,    // *** 分割代入、stateに追加
        {
          email: action.user.email,
          name: action.user.name,
          password: action.user.password
        }
      ]
    default:
      return state
  }
}

export const logined = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_LOGINED_USER': // *** userを追加
      return (
        {
          email: action.user.email,
          name: action.user.name,
          password: action.user.password
        })
    default:
      return state
  }
}
