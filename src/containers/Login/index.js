import { connect } from 'react-redux'
import { asyncLogin } from '../../actions/actions'
import Login from './Login';

import { Form } from 'antd';

function mapStateToProps(state) {
  return state
}


function mapDispatchToProps(dispatch) {
  return {
    onSubmit : values => {
        dispatch(asyncLogin(values))
    }
  }
}

let myLogin = connect(mapStateToProps, mapDispatchToProps)(Login)
myLogin = Form.create({ name: 'login_form' })(myLogin);

export default myLogin
