import React from 'react';
import { Form, Input, Icon, Button } from 'antd';

class Login extends React.Component {

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    console.log(this.props)
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.onSubmit( values )
      }
    })
  }

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const emailError = isFieldTouched('email') && getFieldError('email');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    const buttonDisable = getFieldError('email') || getFieldError('password')

    return(
      <Form onSubmit={this.handleSubmit} className="form-register-containers">
        <h1 className="center">
          ログイン
        </h1>
        <Form.Item label="メールアドレス" validateStatus={emailError ? 'error' : ''} help={emailError || ''}>
          {getFieldDecorator('email', {
            rules: [ {type: 'email', message: 'The input is not valid E-mail!',},
                     { required: true, message: 'Please input your email!' }],
            })(
              <Input
                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="email"
              />,
          )}
        </Form.Item>

        <Form.Item label="パスワード" validateStatus={passwordError ? 'error' : ''} help={passwordError || ''}>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password!' }],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="password"
              />,
          )}
        </Form.Item>


        <Form.Item className="center">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-submit"
            disabled = {buttonDisable}
          >
            ログイン
          </Button>
        </Form.Item>
      </Form>
    )
  }
}


export default Login
