import React from 'react';
import { Form, Input, Icon, Button } from 'antd';

class Register extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    console.log(this.props)
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Submit OK: ', values);
        this.props.onSubmit( values )
      } else {
        console.log('Submit NG: ', values);
      }
    })
  }

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const emailError = isFieldTouched('email') && getFieldError('email');
    const nameError = isFieldTouched('name') && getFieldError('name');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    const confirmError = isFieldTouched('confirm') && getFieldError('confirm');
    const buttonDisable = getFieldError('email') || getFieldError('name') || getFieldError('password') || getFieldError('confirm')

    return(
      <Form onSubmit={this.handleSubmit} className="form-register-containers">
        <h1 className="center">
          ユーザ登録
        </h1>

        <Form.Item label="メールアドレス" validateStatus={emailError ? 'error' : ''} help={emailError || ''}>
          {getFieldDecorator('email', {
            rules: [ {type: 'email', message: 'The input is not valid E-mail!',},
                     { required: true, message: 'Please input your email!' }],
            })(
              <Input
                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="email"
              />,
          )}
        </Form.Item>

        <Form.Item label="パスワード" validateStatus={passwordError ? 'error' : ''} help={passwordError || ''}>
          {getFieldDecorator('password', {
            rules: [ { required: true, message: 'Please input your password!' },
                     { validator: this.validateToNextPassword,} ],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="password"
              />,
          )}
        </Form.Item>

        <Form.Item label="確認パスワード" validateStatus={confirmError ? 'error' : ''} help={confirmError || ''}>
          {getFieldDecorator('confirm', {
            rules: [ { required: true, message: 'Please input your confirmPassword!' },
                     { validator: this.compareToFirstPassword,} ],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="confirmPassword"
                onBlur={this.handleConfirmBlur} 
              />,
          )}
        </Form.Item>

        <Form.Item label="名前" validateStatus={nameError ? 'error' : ''} help={nameError || ''}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please input your name!' }],
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="name"
              />,
          )}
        </Form.Item>

        <Form.Item className="center">
          <Button
            type="primary"
            htmlType="submit"
            className="btn-submit"
            disabled = {buttonDisable}
          >
            ユーザ登録
          </Button>
        </Form.Item>
      </Form>
    )
  }
}


export default Register
