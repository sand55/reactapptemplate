import { connect } from 'react-redux'
import { asyncAddUser } from '../../actions/actions'
import Register from './Register';

import { Form } from 'antd';
// import { push } from 'connected-react-router';

function mapStateToProps(state) {
  return state
}

// function mapDispatchToProps(dispatch, ownProps) {
function mapDispatchToProps(dispatch) {
  return {
    onSubmit : values => {
        dispatch(asyncAddUser(values))
    }// ,
    // ...ownProps
  }
}

let myRegister = connect(mapStateToProps, mapDispatchToProps)(Register)
myRegister = Form.create({ name: 'register_form' })(myRegister);

export default myRegister
